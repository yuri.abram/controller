package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	httpSwagger "github.com/swaggo/http-swagger/v2"
	"gitlab/controller/internal/infrastructure/component"
	"gitlab/controller/internal/modules"
	"gitlab/controller/internal/modules/auth/service"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	geocode := controllers.Geo
	authjwt := controllers.Auth

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(service.TokenAuth))
		r.Use(jwtauth.Authenticator(service.TokenAuth))

		r.Post("/api/address/search", geocode.SearchResponding) //search.go
		r.Post("/api/address/geocode", geocode.GeoCoding)       //geocode.go
	})

	// Public routes
	r.Group(func(r chi.Router) {

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
		))

		r.Post("/api/login", authjwt.Login)
		r.Post("/api/register", authjwt.Register)

	})
	return r
}
