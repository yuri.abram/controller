package service

import (
	"context"
	"encoding/json"
)

type GeocodeRequest struct {
	Lat string `json:"lat" example:"55.753994" validate:"required"`
	Lng string `json:"lon" example:"37.620003" validate:"required"`
}

type GeocodeResponse struct {
	Addresses []*Address `json:"suggestions"`
}

// Geocode
// @summary 		Получить адрес по координатам
// @Description 	Получить адрес по координатам
// @Tags 			geocode
// @Accept 			json
// @Produce 		json
// @Security 		Bearer
//
// param 	param name, param type, data type, is mandatory?, comment attribute(optional)
//
// @Param			request body 	service.GeocodeRequest true "Geocode request"
// @Success 		200 {object} 	service.GeocodeResponse
// @Failure 		400	{string}	string "Bad request"
// @Failure 		401	{string}	string "Unauthorized"
// @Failure 		500	{string}	string "Internal server error"
// @Router 			/api/address/geocode [post]
func (g *GeoService) Geocode(ctx context.Context, rq *GeocodeRequest) GeocodeResponse {

	// doRequest() from apiclient.go
	resp, _ := doRequest(rq)

	var geocodeResponse GeocodeResponse
	_ = json.NewDecoder(resp.Body).Decode(&geocodeResponse)

	// todo проверку ошибок?
	/*if err != nil {
		http.Error(w, "Error decoding response body: "+err.Error(), http.StatusInternalServerError)
		return GeocodeResponse{}
	}*/

	return geocodeResponse

}
