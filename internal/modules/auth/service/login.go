package service

import (
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

// Login - login user
// @summary 		Логин пользователя
// @description 	Логин пользователя.
// @Tags 			auth
// @Accept 			json
// @Produce 		json
// @Param 			request body service.UserReq true "User request"
// @success 200 {string} string "User logged in"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Invalid credentials"
// @Router /api/login [post]
func (db *UsersDB) Login(ctx context.Context, user UserReq) (token string, err error) {

	// проверка пароля и что пользователь существует
	if _, ok := db.users[user.Username]; !ok || bcrypt.CompareHashAndPassword([]byte(db.users[user.Username]), []byte(user.Password)) != nil {
		return "", fmt.Errorf("invalid credentials")
	}

	_, tokenString, _ := TokenAuth.Encode(map[string]interface{}{"username": user.Username, "hash": db.users[user.Username]})

	return tokenString, nil
}
