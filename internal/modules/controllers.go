package modules

import (
	"gitlab/controller/internal/infrastructure/component"
	acontroller "gitlab/controller/internal/modules/auth/controller"
	gcontroller "gitlab/controller/internal/modules/geo/controller"
)

type Controllers struct {
	Geo  gcontroller.GeoResponder
	Auth acontroller.Auther
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	geoController := gcontroller.NewGeo(services.GeoService, components)
	authController := acontroller.NewAuth(services.Auth, components)

	return &Controllers{
		Geo:  geoController,
		Auth: authController,
	}
}
