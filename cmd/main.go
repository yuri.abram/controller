package main

import (
	"github.com/joho/godotenv"
	"gitlab/controller/config"
	_ "gitlab/controller/docs"
	"gitlab/controller/internal/infrastructure/logs"
	"gitlab/controller/run"
	"os"
)

// @title задача Добавление контроллера в геосервис
// @version 2.0
// @description  Геосервис API с авторизацией
// @host localhost:8080
// @BasePath /

//	@securityDefinitions.apikey	Bearer
//	@in							header
//	@name						Authorization
//	@description The following syntax must be used in the 'Authorization' header: Bearer: xxxxxx.yyyyyyy.zzzzzz

func main() {

	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}

/*
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	// Protected routes
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(jwt.TokenAuth))
		r.Use(jwtauth.Authenticator(jwt.TokenAuth))

		r.Post("/api/address/search", geoservice.Search)   //search.go
		r.Post("/api/address/geocode", geoservice.Geocode) //geocode.go
	})

	// Public routes
	r.Group(func(r chi.Router) {

		r.Get("/swagger/*", httpSwagger.Handler(
			httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
		))

		r.Post("/api/login", jwt.LoginHandler)
		r.Post("/api/register", jwt.RegistrationHandler)

	})

	// Создание HTTP-сервера
	server := &http.Server{
		Addr:         ":8080",
		Handler:      r,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	// Создание канала для получения сигналов остановки
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// Запуск сервера в отдельной горутине
	go func() {
		log.Println("Starting server...")
		log.Println("Swagger: http://localhost:8080/swagger/")
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("Server error: %v", err)
		}
	}()

	// Ожидание сигнала остановки
	<-shutdown

	// Создание контекста с таймаутом для graceful shutdown
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Остановка сервера с использованием graceful shutdown
	log.Println("Stopping server...")
	server.Shutdown(ctx)

	log.Println("Server stopped gracefully")

*/
